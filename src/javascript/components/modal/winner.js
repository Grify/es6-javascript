import {showModal} from './modal';

import {createFighterImage} from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const objArgument = {
    title: "Winner: " + fighter.name,
    bodyElement: createFighterImage(fighter),
    onClose: function() {window.location.reload();}
  }
  showModal(objArgument);
}
