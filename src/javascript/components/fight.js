import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let isBlockFirstFighter = false;
    let isBlockSecondFighter = false;
    let currentHPFirstFighter = 100;
    let currentHPSecondFighter = 100;
    let timeLastCritFirstFighter = new Date(2010, 0, 0);
    let timeLastCritSecondFighter = new Date(2010, 0, 0);
    let downKeys = {};

    document.addEventListener('keydown', function(event) {
      //FirstFigher attack
      if(event.code == controls.PlayerOneAttack && !isBlockSecondFighter && !isBlockFirstFighter) {
        const damagePersent = (100 * getDamage(firstFighter, secondFighter)) / secondFighter.health;
        if(currentHPSecondFighter - damagePersent <= 0) {
          currentHPSecondFighter = 0;
          document.getElementById('right-fighter-indicator').style.width = currentHPSecondFighter + '%';
          resolve(firstFighter);
        } else {
          currentHPSecondFighter -= damagePersent;
          document.getElementById('right-fighter-indicator').style.width = currentHPSecondFighter + '%';
        }
      }
      //SecondFigher attack
      if(event.code == controls.PlayerTwoAttack && !isBlockFirstFighter && !isBlockSecondFighter) {
        const damagePersent = (100 * getDamage(secondFighter, firstFighter)) / firstFighter.health;
        if(currentHPFirstFighter - damagePersent <= 0) {
          currentHPFirstFighter = 0;
          document.getElementById('left-fighter-indicator').style.width = currentHPFirstFighter + '%';
          resolve(secondFighter);
        } else {
          currentHPFirstFighter -= damagePersent;
          document.getElementById('left-fighter-indicator').style.width = currentHPFirstFighter + '%';
        }
      }
      //FirstFighter set block
      if(event.code == controls.PlayerOneBlock) {
        isBlockFirstFighter = true;
      }
      //SecondFighter set block
      if(event.code == controls.PlayerTwoBlock) {
        isBlockSecondFighter = true;
      }
      //FirstFighter CriticalHit
      if(event.code == controls.PlayerOneCriticalHitCombination[0]
        || event.code == controls.PlayerOneCriticalHitCombination[1]
        || event.code == controls.PlayerOneCriticalHitCombination[2]) {

        downKeys[event.code] = true;

        if(downKeys[controls.PlayerOneCriticalHitCombination[0]]
          && downKeys[controls.PlayerOneCriticalHitCombination[1]]
          && downKeys[controls.PlayerOneCriticalHitCombination[2]]) {
          //Time check
          const now = new Date;
          if(now.getTime() - timeLastCritFirstFighter.getTime() >= 10 * 1000) {
            timeLastCritFirstFighter = now;
            const damage = 2 * firstFighter.attack;
            const damagePersent = damage > 0 ? (100 * damage) / secondFighter.health : 0;
            if(currentHPSecondFighter - damagePersent <= 0) {
              document.getElementById('right-fighter-indicator').style.width = 0 + '%';
              resolve(firstFighter);
            } else {
              currentHPSecondFighter  = currentHPSecondFighter - damagePersent;
              document.getElementById('right-fighter-indicator').style.width = currentHPSecondFighter + '%';
            }
          }
        }
      }
      //SecondFighter CriticalHit
      if(event.code == controls.PlayerTwoCriticalHitCombination[0]
        || event.code == controls.PlayerTwoCriticalHitCombination[1]
        || event.code == controls.PlayerTwoCriticalHitCombination[2]) {

        downKeys[event.code] = true;

        if(downKeys[controls.PlayerTwoCriticalHitCombination[0]]
          && downKeys[controls.PlayerTwoCriticalHitCombination[1]]
          && downKeys[controls.PlayerTwoCriticalHitCombination[2]]) {
          //Time check
          const now = new Date;
          if(now.getTime() - timeLastCritSecondFighter.getTime() >= 10 * 1000) {
            timeLastCritSecondFighter = now;
            const damage = 2 * secondFighter.attack;
            const damagePersent = damage > 0 ? (100 * damage) / firstFighter.health : 0;
            if(currentHPFirstFighter - damagePersent <= 0) {
              document.getElementById('left-fighter-indicator').style.width = 0 + '%';
              resolve(secondFighter);
            } else {
              currentHPFirstFighter  = currentHPFirstFighter - damagePersent;
              document.getElementById('left-fighter-indicator').style.width = currentHPFirstFighter + '%';
            }
          }
        }
      }

      
    });

    document.addEventListener('keyup', function(event) {
      //FirstFighter unset block
      if(event.code == controls.PlayerOneBlock) {
        isBlockFirstFighter = false;
      }
      //SecondFighter unset block
      if(event.code == controls.PlayerTwoBlock) {
        isBlockSecondFighter = false;
      }
      //FirstFighter unset CriticalHit
      if(event.code == controls.PlayerOneCriticalHitCombination[0]
        || event.code == controls.PlayerOneCriticalHitCombination[1]
        || event.code == controls.PlayerOneCriticalHitCombination[2]) {

        downKeys[event.code] = false; 
      }
      //SecondFighter unset CriticalHit
      if(event.code == controls.PlayerTwoCriticalHitCombination[0]
        || event.code == controls.PlayerTwoCriticalHitCombination[1]
        || event.code == controls.PlayerTwoCriticalHitCombination[2]) {

        downKeys[event.code] = false; 
      }
    });
    });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = (getHitPower(attacker) - getBlockPower(defender) < 0) ? 0 : (getHitPower(attacker) - getBlockPower(defender));
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const attack = fighter.attack;
  const criticalHitChance = Math.random() + 1;
  const power = attack * criticalHitChance
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const defense = fighter.defense;
  const dodgeChance = Math.random() + 1;
  const power = defense * dodgeChance;
  return power;
}
