import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (typeof fighter !== 'undefined') {
    const {name, health, attack, defense} = fighter
    const fighterImage = createFighterImage(fighter);
    let fighterInfo = document.createElement('ul');
    fighterInfo.innerHTML = `<li>Name: ${name}</li><li>Health: ${health}</li><li>Attack: ${attack}</li><li>Defense: ${defense}</li>`;

    fighterElement.append(fighterInfo, fighterImage);
  }
  

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
